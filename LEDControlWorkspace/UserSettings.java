/*
 * UserSettings.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package LEDControl;

import net.rim.device.api.util.Persistable;


/**
 * 
 */
class UserSettings implements Persistable {
    
    public int _ledredvalue = 0;
    public int _ledgreenvalue = 255;
    public int _ledbluevalue = 0;
    public int _ledontime = 500;
    public int _ledofftime = 250;
    
    public int _ledtorchredvalue = 255;
    public int _ledtorchgreenvalue = 255;
    public int _ledtorchbluevalue = 255;
    
    public int _ledrandmaxred = 255;
    public int _ledrandmaxgreen = 255;
    public int _ledrandmaxblue = 255;
    public int _ledrandchangems = 40;
    
    public String _usecustomcoverage = "no";
    public String _usefadingcoverage = "no";
    public int _coveragefadespeed = 20;
    public int _checkcoverageinterval = 3000;
    public int _checkcoverageledon = 30;
        
    public int _nocoveragered = 255;
    public int _nocoveragegreen = 0;
    public int _nocoverageblue = 0;
    
    public int _lowcoveragered = 255;
    public int _lowcoveragegreen = 70;
    public int _lowcoverageblue = 0;
    
    public int _medcoveragered = 0;
    public int _medcoveragegreen = 255;
    public int _medcoverageblue = 0;
    
    public int _highcoveragered = 0;
    public int _highcoveragegreen = 0;
    public int _highcoverageblue = 255;
    
    UserSettings() {    
    
    }
} 
