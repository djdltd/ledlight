/*
 * LEDControl.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package LEDControl;

import java.util.*;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.LED;
import net.rim.device.api.system.RadioInfo;

/**
 * 
 */
class LEDControl  extends UiApplication {
    
    // Toggle mode
    private EditField _efred;
    private EditField _efgreen;
    private EditField _efblue;
    private EditField _efontime;
    private EditField _efofftime;
    
    // Torch mode
    private EditField _eftorchred;
    private EditField _eftorchgreen;
    private EditField _eftorchblue;
    
    // Randomise mode
    private EditField _efmaxred;
    private EditField _efmaxgreen;
    private EditField _efmaxblue;
    private EditField _efswitchtime;    
    
    // Custom coverage indicator
    private ObjectChoiceField _efusecoverage;
    private ObjectChoiceField _efusecoveragefade;
    private EditField _efcoverageinterval;
    private EditField _efcoverageledon;
    private EditField _efcoveragefadespeed;
    
    private EditField _nocoveragered;
    private EditField _nocoveragegreen;
    private EditField _nocoverageblue;
    
    private EditField _lowcoveragered;
    private EditField _lowcoveragegreen;
    private EditField _lowcoverageblue;
    
    private EditField _medcoveragered;
    private EditField _medcoveragegreen;
    private EditField _medcoverageblue;

    private EditField _highcoveragered;
    private EditField _highcoveragegreen;
    private EditField _highcoverageblue;
    
    String[] stryesno = new String[2];
    
    private UserSettings _settings = new UserSettings();
    private PersistentObject _persist;
    private boolean _ledon = false;
    private boolean _coverageon = false;
    private boolean _randomon = false;
    private boolean _randomstop = false;
    private boolean _coveragestop = false;
    private static Random _rnd = new Random();
    private RandomiseThread _randomisethread;
    private CoverageThread _coveragethread;
    
    private MenuItem _ledMenuItem = new MenuItem("Toggle LED On/Off", 100, 10) 
    {
        public void run()
        {
                        
            //Dialog.alert("Contact Saved!" + dtCurrent.toString());
            //setLocalcontact();
            setLocalsettings();
            _persist.commit();
            
            if (_ledon == false) {
                _ledon = true;
                FlashLED();
            } else {
                _ledon = false;
                StopLED();
            }
        }
    };
    
    private MenuItem _ledTorchMenuItem = new MenuItem("Torch Mode On/Off", 100, 10) 
    {
        public void run()
        {
                        
            //Dialog.alert("Contact Saved!" + dtCurrent.toString());
            //setLocalcontact();
            setLocalsettings();
            _persist.commit();
            
            if (_ledon == false) {
                _ledon = true;
                TorchLED();
            } else {
                _ledon = false;
                StopLED();
            }
        }
    };
    
    private MenuItem _ledRandomMenuItem = new MenuItem("Randomize On/Off", 100, 10) 
    {
        public void run()
        {
                        
            //Dialog.alert("Contact Saved!" + dtCurrent.toString());
            //setLocalcontact();
            setLocalsettings();
            _persist.commit();
            
            if (_randomon == false) {
                _randomon = true;
                _ledon = true;
                _randomisethread = new RandomiseThread();
                _randomstop = false;                
                _randomisethread.start();
            } else {
                _randomon = false;
                _ledon = false;
                _randomisethread.stop();
                StopLED();
            }
        }
    };
    
    private MenuItem _ledCoverageMenuItem = new MenuItem("Custom Coverage LED On/Off", 100, 10) 
    {
        public void run()
        {
                        
            //Dialog.alert("Contact Saved!" + dtCurrent.toString());
            //setLocalcontact();
            setLocalsettings();
            _persist.commit();
            
            if (_coverageon == false) {
                _coverageon = true;
                _coveragethread = new CoverageThread();
                _coveragestop = false;
                _coveragethread.start();
            } else {
                _coverageon = false;
                _coveragethread.stop();
                StopLED();
            }
        }
    };
    
    
    private MenuItem _aboutMenuItem = new MenuItem("About", 100, 10) 
    {                
        public void run()
        {                                    
            Dialog.inform("LEDlight v1.1.\n\nCopyright (c) 2008-2009 CedeSoft Ltd.\n\nAll Rights Reserved.");                                       
        }
    };
    
    
    private MenuItem _shutdownMenuItem = new MenuItem("Exit", 100, 10) 
    {                
        public void run()
        {            
            System.exit(0);                             
        }
    };
    
    LEDControl() {
        LEDControlScreen screen = new LEDControlScreen();
        pushScreen(screen); 
    }
    
    public static void main(String[] args)
    {
        
        // Create a new instance of the application and start 
        // the application on the event thread.
        LEDControl ledcontrol  = new LEDControl();       
        ledcontrol.enterEventDispatcher(); 
    }
    
    public int getColorvalue(int red, int green, int blue)
    {                            
            byte [] intByte = new byte [4];
            
            intByte[0] = (byte) blue;
            intByte[1] = (byte) green;
            intByte[2] = (byte) red;
            intByte[3] = (byte) 0;
            
            int fromByte = 0;
            
            for (int i = 0; i < 4; i++)
            {
                int n = (intByte[i] < 0 ? (int)intByte[i] + 256 : (int)intByte[i]) << (8 * i);                
                fromByte += n;
            }
            
            return fromByte;        
    }
    
    public void FlashLED()
    {
        setLocalsettings();
        //LED.setColorConfiguration(500, 250, 0x00ffff00);
        LED.setColorConfiguration(_settings._ledontime, _settings._ledofftime, getColorvalue(_settings._ledredvalue, _settings._ledgreenvalue, _settings._ledbluevalue));        
        
        //LED.setConfiguration( 500, 250, LED.BRIGHTNESS_100 );
        LED.setState( LED.STATE_BLINKING );
    }
    
    public void StopLED()
    {
        LED.setState( LED.STATE_OFF );
    }
        
    public void TorchLED()
    {
        setLocalsettings();
        //LED.setColorConfiguration(500, 250, 0x00ffff00);
        LED.setColorConfiguration(_settings._ledontime, _settings._ledofftime, getColorvalue(_settings._ledtorchredvalue, _settings._ledtorchgreenvalue, _settings._ledtorchbluevalue));        
        
        //LED.setConfiguration( 500, 250, LED.BRIGHTNESS_100 );
        LED.setState( LED.STATE_ON );
    }
    
    int getSelcolour(String strValue)
    {
        int icolour = Integer.parseInt(strValue);
        if (icolour >= 0 && icolour <=255) {
            return icolour;
        } else {
            Dialog.alert("Colour values must be between 0 and 255.");
            return 1;
        }
    }
    
    int getSeltime(String strValue)
    {
        int itime = Integer.parseInt(strValue);
        if (itime > 0 && itime <= 20000) {
            return itime;            
        } else {
            Dialog.alert("Time values must be between 1 and 20000.");
            return 250;
        }
    }
    
    public void setLocalsettings()
    {   
        // Toggle
        _settings._ledredvalue = getSelcolour(_efred.getText());
        _settings._ledgreenvalue = getSelcolour(_efgreen.getText());
        _settings._ledbluevalue = getSelcolour(_efblue.getText());
        _settings._ledontime = getSeltime(_efontime.getText());
        _settings._ledofftime = getSeltime(_efofftime.getText());
        
        // Torch
        _settings._ledtorchredvalue = getSelcolour(_eftorchred.getText());
        _settings._ledtorchgreenvalue = getSelcolour(_eftorchgreen.getText());
        _settings._ledtorchbluevalue = getSelcolour(_eftorchblue.getText());
        
        // Randomise
        _settings._ledrandmaxred = getSelcolour(_efmaxred.getText());
        _settings._ledrandmaxgreen = getSelcolour(_efmaxgreen.getText());
        _settings._ledrandmaxblue = getSelcolour(_efmaxblue.getText());
        _settings._ledrandchangems = getSeltime(_efswitchtime.getText());
        
        // Coverage
        _settings._usecustomcoverage = getMultichoice(_efusecoverage, stryesno);
        _settings._checkcoverageinterval = getSeltime(_efcoverageinterval.getText());
        _settings._checkcoverageledon = getSeltime(_efcoverageledon.getText());
        _settings._usefadingcoverage = getMultichoice(_efusecoveragefade, stryesno);
        _settings._coveragefadespeed = getSeltime(_efcoveragefadespeed.getText());
        
        _settings._nocoveragered = getSelcolour(_nocoveragered.getText());
        _settings._nocoveragegreen = getSelcolour(_nocoveragegreen.getText());
        _settings._nocoverageblue = getSelcolour(_nocoverageblue.getText());

        _settings._lowcoveragered = getSelcolour(_lowcoveragered.getText());
        _settings._lowcoveragegreen = getSelcolour(_lowcoveragegreen.getText());
        _settings._lowcoverageblue = getSelcolour(_lowcoverageblue.getText());
        
        _settings._medcoveragered = getSelcolour(_medcoveragered.getText());
        _settings._medcoveragegreen = getSelcolour(_medcoveragegreen.getText());
        _settings._medcoverageblue = getSelcolour(_medcoverageblue.getText());        
        
        _settings._highcoveragered = getSelcolour(_highcoveragered.getText());
        _settings._highcoveragegreen = getSelcolour(_highcoveragegreen.getText());
        _settings._highcoverageblue = getSelcolour(_highcoverageblue.getText());        
        
        
        
    }
    
    public void setLocalform()
    {
        // Toggle
        _efred.setText(String.valueOf(_settings._ledredvalue));
        _efgreen.setText(String.valueOf(_settings._ledgreenvalue));
        _efblue.setText(String.valueOf(_settings._ledbluevalue));
        _efontime.setText(String.valueOf(_settings._ledontime));
        _efofftime.setText(String.valueOf(_settings._ledofftime));
        
        //Torch
        _eftorchred.setText(String.valueOf(_settings._ledtorchredvalue));
        _eftorchgreen.setText(String.valueOf(_settings._ledtorchgreenvalue));
        _eftorchblue.setText(String.valueOf(_settings._ledtorchbluevalue));
        
        // Randomise
       _efmaxred.setText(String.valueOf(_settings._ledrandmaxred));
       _efmaxgreen.setText(String.valueOf(_settings._ledrandmaxgreen));
       _efmaxblue.setText(String.valueOf(_settings._ledrandmaxblue));
       _efswitchtime.setText(String.valueOf(_settings._ledrandchangems));
       
       // Coverage
       setMultichoice(_efusecoverage, _settings._usecustomcoverage, stryesno);
       _efcoverageinterval.setText(String.valueOf(_settings._checkcoverageinterval));
       _efcoverageledon.setText(String.valueOf(_settings._checkcoverageledon));
       setMultichoice(_efusecoveragefade, _settings._usefadingcoverage, stryesno);
       _efcoveragefadespeed.setText(String.valueOf(_settings._coveragefadespeed));
       
       _nocoveragered.setText(String.valueOf(_settings._nocoveragered));
       _nocoveragegreen.setText(String.valueOf(_settings._nocoveragegreen));
       _nocoverageblue.setText(String.valueOf(_settings._nocoverageblue));

       _lowcoveragered.setText(String.valueOf(_settings._lowcoveragered));
       _lowcoveragegreen.setText(String.valueOf(_settings._lowcoveragegreen));
       _lowcoverageblue.setText(String.valueOf(_settings._lowcoverageblue));

       _medcoveragered.setText(String.valueOf(_settings._medcoveragered));
       _medcoveragegreen.setText(String.valueOf(_settings._medcoveragegreen));
       _medcoverageblue.setText(String.valueOf(_settings._medcoverageblue));
 
       _highcoveragered.setText(String.valueOf(_settings._highcoveragered));
       _highcoveragegreen.setText(String.valueOf(_settings._highcoveragegreen));
       _highcoverageblue.setText(String.valueOf(_settings._highcoverageblue));
       
       
       
        //_efchoice.set
    }
    
    public void setMultichoice(ObjectChoiceField myfield, String strChoice, String[] mychoices)
    {
        if (strChoice.trim() == "") {
            myfield.setSelectedIndex(0);
        } else {
            
            int c = 0;
            String strcur = "";
            
            for(c=0;c<mychoices.length;c++)
            {
                strcur = mychoices[c];
                if (strcur.compareTo(strChoice) == 0) {
                    myfield.setSelectedIndex(c);
                    return;
                }
            }            
        }
    }
    
    public String getMultichoice (ObjectChoiceField myfield, String[] mychoices)
    {
        int index = myfield.getSelectedIndex();
        
        return mychoices[index];
    }
    
    public void loadSettings ()
    {
         // Hash of "CedeSMS".
         //9504d44735f128def178e9a3a10185f1
         //10861765ee984b837fa71352f67ac692 // has of led fun
         long KEY =  0x10861765ee984b83L;
         _persist = PersistentStore.getPersistentObject( KEY );
         _settings = (UserSettings) _persist.getContents();
         if( _settings == null ) {
             _settings = new UserSettings();
             _persist.setContents( _settings );
             //persist.commit()
         }
    }
    
    public static  int GetRand (int Max)
    {
        return _rnd.nextInt(Max);
    }
    
    private class LEDControlScreen extends MainScreen {
        
        LEDControlScreen ()
        {
            setTitle(new LabelField("LEDlight" , LabelField.ELLIPSIS | LabelField.USE_ALL_WIDTH));
        
            _efred = new EditField("Toggle Color - Red Amount: ", "", 3, EditField.FILTER_INTEGER);
            _efgreen = new EditField("Toggle Color - Green Amount: ", "", 3, EditField.FILTER_INTEGER);
            _efblue = new EditField("Toggle Color - Blue Amount: ", "", 3, EditField.FILTER_INTEGER);
            _efontime = new EditField("Toggle Flash - On time (ms): ","",4, EditField.FILTER_INTEGER);
            _efofftime = new EditField("Toggle Flash - Off time (ms): ","",4, EditField.FILTER_INTEGER);
            
            _eftorchred = new EditField("Torch Color - Red Amount: ", "", 3, EditField.FILTER_INTEGER);
            _eftorchgreen = new EditField("Torch Color - Green Amount: ", "", 3, EditField.FILTER_INTEGER);
            _eftorchblue = new EditField("Torch Color - Blue Amount: ", "", 3, EditField.FILTER_INTEGER);
            
            _efmaxred = new EditField("Randomize - Max Red Amount: ", "", 3, EditField.FILTER_INTEGER);
            _efmaxgreen  = new EditField("Randomize - Max Green Amount: ", "", 3, EditField.FILTER_INTEGER);
            _efmaxblue = new EditField("Randomize - Max Blue Amount: ", "", 3, EditField.FILTER_INTEGER);
            _efswitchtime = new EditField("Randomize - Switch time (ms): ", "", 4, EditField.FILTER_INTEGER);
            
            stryesno[0] = "yes";
            stryesno[1] = "no";
            
            _efusecoverage = new ObjectChoiceField("Use custom coverage indicator: ", stryesno);
            _efusecoveragefade = new ObjectChoiceField("Fade coverage indicator: ", stryesno);
            
            _efcoveragefadespeed = new EditField("Coverage - LED fade speed (ms): ", "", 4, EditField.FILTER_INTEGER);
            _efcoverageinterval = new EditField("Coverage - Check interval (ms): ", "", 4, EditField.FILTER_INTEGER);
            _efcoverageledon = new EditField("Coverage - LED on time (ms): ", "", 4, EditField.FILTER_INTEGER);
                
            _nocoveragered = new EditField("No Coverage - Red Amount: ", "", 3, EditField.FILTER_INTEGER);
            _nocoveragegreen = new EditField("No Coverage - Green Amount: ", "", 3, EditField.FILTER_INTEGER);
            _nocoverageblue = new EditField("No Coverage - Blue Amount: ", "", 3, EditField.FILTER_INTEGER);
                
            _lowcoveragered = new EditField("Low Coverage - Red Amount: ", "", 3, EditField.FILTER_INTEGER);
            _lowcoveragegreen = new EditField("Low Coverage - Green Amount: ", "", 3, EditField.FILTER_INTEGER);
            _lowcoverageblue = new EditField("Low Coverage - Blue Amount: ", "", 3, EditField.FILTER_INTEGER);
                
            _medcoveragered = new EditField("Medium Coverage - Red Amount: ", "", 3, EditField.FILTER_INTEGER);
            _medcoveragegreen = new EditField("Medium Coverage - Green Amount: ", "", 3, EditField.FILTER_INTEGER);
            _medcoverageblue = new EditField("Medium Coverage - Blue Amount: ", "", 3, EditField.FILTER_INTEGER);
            
            _highcoveragered = new EditField("High Coverage - Red Amount: ", "", 3, EditField.FILTER_INTEGER);
            _highcoveragegreen = new EditField("High Coverage - Green Amount: ", "", 3, EditField.FILTER_INTEGER);
            _highcoverageblue = new EditField("High Coverage - Blue Amount: ", "", 3, EditField.FILTER_INTEGER);
            
            
            add(_efred);
            add(_efgreen);
            add(_efblue);
            add(_efontime);
            add(_efofftime);
            add(new SeparatorField());
            add(_eftorchred);
            add(_eftorchgreen);
            add(_eftorchblue);
            add(new SeparatorField());
            add(_efmaxred);
            add(_efmaxgreen);
            add(_efmaxblue);
            add(_efswitchtime);
            add(new SeparatorField());
            add(_efusecoverage);
            add(_efusecoveragefade);
            add(_efcoveragefadespeed);
            add(_efcoverageinterval);
            add(_efcoverageledon);
            add(new SeparatorField());
            add(_nocoveragered);
            add(_nocoveragegreen);
            add(_nocoverageblue);
            add(new SeparatorField());
            add(_lowcoveragered);
            add(_lowcoveragegreen);
            add(_lowcoverageblue);
            add(new SeparatorField());
            add(_medcoveragered);
            add(_medcoveragegreen);
            add(_medcoverageblue);            
            add(new SeparatorField());
            add(_highcoveragered);
            add(_highcoveragegreen);
            add(_highcoverageblue);            
            
            
            addMenuItem(_ledMenuItem);
            addMenuItem(_ledTorchMenuItem);
            addMenuItem(_ledRandomMenuItem);
            addMenuItem(_ledCoverageMenuItem);
            addMenuItem(_aboutMenuItem);
            addMenuItem(_shutdownMenuItem);
            
            loadSettings();
            setLocalform();         
            
            if (_settings._usecustomcoverage.compareTo("yes") == 0) {
                if (_coverageon == false) {
                    _coverageon = true;
                    _coveragethread = new CoverageThread();
                    _coveragestop = false;
                    _coveragethread.start();
                    
                } 
            }
        }
        
        
        public boolean onSave()
        {
            setLocalsettings();
            _persist.commit();
            return true;
        }
        
        public void close()
        {
            //_listener.stop();
            //_sender.stop();
            
            //super.close();
            Dialog.alert("Press the Red Hangup button to get back to the Blackberry home screen.");           
        }        
    }
    
    
    
    private class RandomiseThread extends Thread
    {

        private synchronized void stop()
        {
           _randomstop = true;
        }

        public void run ()
        {               
            int rndred = 0;
            int rndgreen = 0;
            int rndblue = 0;
                        
            for (;;)
            {                
                if (_settings._ledrandmaxred > 0) {
                    rndred = GetRand(_settings._ledrandmaxred);
                }
                
                if (_settings._ledrandmaxgreen > 0) {
                    rndgreen = GetRand(_settings._ledrandmaxgreen);
                }
                
                if (_settings._ledrandmaxblue > 0) {
                    rndblue = GetRand(_settings._ledrandmaxblue);
                }
                
                LED.setColorConfiguration(_settings._ledontime, _settings._ledofftime, getColorvalue(rndred, rndgreen, rndblue));        
        
                //LED.setConfiguration( 500, 250, LED.BRIGHTNESS_100 );
                LED.setState( LED.STATE_ON );
                
                try {
                    Thread.sleep (_settings._ledrandchangems);
                }
                catch (InterruptedException ios) { }
                
                if (_randomstop == true) {
                    break;
                }
            }
        }
    }
    
    
    
    private class CoverageThread extends Thread
    {

        private synchronized void stop()
        {
           _coveragestop = true;
        }

        public void run ()
        {               
            int siglevel = 0;
            int targetred = 0;
            int targetgreen = 0;
            int targetblue = 0;
            int r = 0;
            int g = 0;
            int b = 0;
            int c  = 0;
            boolean tr = false;
            boolean tg = false;
            boolean tb = false;
            boolean usefade = false;
            
            for (;;)
            {                
                siglevel = RadioInfo.getSignalLevel();
                
                System.out.println("SIGNAL LEVEL LEDLIGHT:");
                System.out.println(siglevel);
                
                if (_settings._usefadingcoverage.compareTo("yes") == 0) {
                    usefade = true;
                } else {
                    usefade = false;
                }
                
                
                if (_ledon == false) {
                                    
                    if (siglevel == -256)  { // no coverage
                        System.out.println("NO COVERAGE");
                        LED.setColorConfiguration(_settings._ledontime, _settings._ledofftime, getColorvalue(_settings._nocoveragered, _settings._nocoveragegreen, _settings._nocoverageblue));        
                        targetred = _settings._nocoveragered;
                        targetgreen = _settings._nocoveragegreen;
                        targetblue = _settings._nocoverageblue;
                    } else {
                                        
                        if (siglevel <-100) { // low coverage
                            System.out.println("LOW COVERAGE");
                            LED.setColorConfiguration(_settings._ledontime, _settings._ledofftime, getColorvalue(_settings._lowcoveragered, _settings._lowcoveragegreen, _settings._lowcoverageblue));
                            targetred = _settings._lowcoveragered;
                            targetgreen = _settings._lowcoveragegreen;
                            targetblue = _settings._lowcoverageblue;
                        }
                        
                        if (siglevel <=-88 && siglevel >=-100) { // medium coverage
                            System.out.println("MED COVERAGE");
                            LED.setColorConfiguration(_settings._ledontime, _settings._ledofftime, getColorvalue(_settings._medcoveragered, _settings._medcoveragegreen, _settings._medcoverageblue));
                            targetred = _settings._medcoveragered;
                            targetgreen = _settings._medcoveragegreen;
                            targetblue = _settings._medcoverageblue;
                        }
                        
                        if (siglevel >-88) { // high coverage
                            System.out.println("HIGH COVERAGE");
                            LED.setColorConfiguration(_settings._ledontime, _settings._ledofftime, getColorvalue(_settings._highcoveragered, _settings._highcoveragegreen, _settings._highcoverageblue));
                            targetred = _settings._highcoveragered;
                            targetgreen = _settings._highcoveragegreen;
                            targetblue = _settings._highcoverageblue;
                        }
                    }
                    
                    //LED.setColorConfiguration(_settings._ledontime, _settings._ledofftime, getColorvalue(rndred, rndgreen, rndblue));        
            
                    //LED.setConfiguration( 500, 250, LED.BRIGHTNESS_100 );
                    if (usefade == true) {
                        r = 0;
                        g = 0;
                        b = 0;
                        tr = false;
                        tg = false;
                        tb = false;
                        
                        for (c=0;c<255;c++) { // LED FADE ON
                            
                            if (r<targetred) {
                                r++;
                            } else {
                                tr = true;
                            }
                            
                            if (g<targetgreen) {
                                g++;
                            } else {
                                tg = true;
                            }
                            
                            if (b<targetblue) {
                                b++;
                            } else {
                                tb = true;
                            }
                            
                            if (_ledon == false) {
                                LED.setColorConfiguration(_settings._ledontime, _settings._ledofftime, getColorvalue(r, g, b));
                                LED.setState( LED.STATE_ON );                            
                            }
                            
                            try {
                                Thread.sleep (_settings._coveragefadespeed);
                            }
                            catch (InterruptedException ios) { }                            
                            
                            if (tr == true && tg == true && tb == true) {
                                break;
                            }
                        }

                        // LED ON TIME
                        try {
                            Thread.sleep (_settings._checkcoverageledon);
                        }
                        catch (InterruptedException ios) { }
                        
                        tr = false;
                        tg = false;
                        tb = false;
                        
                        // LED FADE OFF
                        for (c=0;c<255;c++) {  // LED FADE OFF
                            
                            if (r>1) {
                                r--;
                            } else {
                                tr = true;
                            }
                            
                            if (g>1) {
                                g--;
                            } else {
                                tg = true;
                            }
                            
                            if (b>1) {
                                b--;
                            } else {
                                tb = true;
                            }
                            
                            if (_ledon == false) {
                                LED.setColorConfiguration(_settings._ledontime, _settings._ledofftime, getColorvalue(r, g, b));
                                LED.setState( LED.STATE_ON );                            
                            }
                            
                            try {
                                Thread.sleep (_settings._coveragefadespeed);
                            }
                            catch (InterruptedException ios) { }                            
                            
                            if (tr == true && tg == true && tb == true) {
                                break;
                            }
                        }
                        
                    } else {
                        LED.setState( LED.STATE_ON );
                    }
                    
                    
                    
                    
                    try {
                        Thread.sleep (_settings._checkcoverageledon);
                    }
                    catch (InterruptedException ios) { }
                    
                    if (_ledon == false) {
                        LED.setState( LED.STATE_OFF);
                    }
                }
                
                try {
                    Thread.sleep (_settings._checkcoverageinterval);
                }
                catch (InterruptedException ios) { }
                
                if (_coveragestop == true) {
                    break;
                }
            }
        }
    }
} 
